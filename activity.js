// Inserting a Single Room
db.hotel.insert({
	name: "single",
	accommodations: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

// Inserting Many Rooms
db.hotel.insertMany([
	{
		name: "double",
		accommodations: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accommodations: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
]);

// Searching for a room name double
db.hotel.find({ name: "double" });

// Updating queen room with available rooms to 0.
db.hotel.updateOne(
	{name: "queen"},
	{$set: {
		rooms_available: 0
	}}
);

// Deleting many rooms which have 0 available
db.hotel.deleteMany({rooms_available:0});